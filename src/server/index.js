const http = require("http");
const fs = require("fs");
const { v4: uuidv4 } = require("uuid");

const uuidFilePath = "../client/uuid.txt";

let readFile = (filename) => {
  return new Promise((resolve, reject) => {
    fs.readFile(filename, (err, data) => {
      if (err) {
        reject(err);
      } else {
        resolve(data);
      }
    });
  });
};

let writeFile = (data, jsonPath) => {
  const jsonString = JSON.stringify(data);
  return new Promise((resolve, reject) => {
    fs.writeFile(jsonPath, jsonString, "utf8", (err) => {
      if (err) {
        reject(err);
      } else {
        resolve("successful written");
      }
    });
  });
};

writeFile({ uuid: uuidv4() }, uuidFilePath)
  .then((data) => console.log(`uuid is ${data} to uuid.txt`))
  .catch((err) => console.error("error in writing file occurred", err));

const server = http.createServer((request, response) => {
  let urlChecker = request.url;
  let splitUrl = urlChecker.split("/");
  let urlNumber;
  if (splitUrl.length > 2) {
    urlNumber = splitUrl[splitUrl.length - 1];
    let urlName = splitUrl[splitUrl.length - 2];
    if (urlName === "delay") {
      urlChecker = "/delay";
    } else if (urlName === "status") {
      urlChecker = "/status";
    }
  }
  switch (urlChecker) {
    case "/html": {
      readFile("../client/index.html")
        .then((data) => {
          response.writeHead(200, {
            "Content-Type": "text/html",
          });
          response.write(data);
          response.end();
        })
        .catch((err) => {
          response.write(err);
          response.end();
        });
      break;
    }

    case "/json": {
      readFile("../client/data.json")
        .then((data) => {
          response.writeHead(200, {
            "Content-Type": "text/json",
          });
          response.write(data);
          response.end();
        })
        .catch((err) => {
          response.write(err);
          response.end();
        });
      break;
    }

    case "/uuid": {
      readFile("../client/uuid.txt")
        .then((data) => {
          response.writeHead(200, {
            "Content-Type": "text/txt",
          });
          response.write(data);
          response.end();
        })
        .catch((err) => {
          response.write(err);
          response.end();
        });
      break;
    }

    case "/delay": {
      setTimeout(() => {
        response.writeHead(200);
        response.write("<h1>Please check status code</h1>");
        response.end();
      }, Number(urlNumber) * 1000);
      break;
    }

    case "/status": {
      response.writeHead(urlNumber);
      response.write("<h1>Please check status code</h1>");
      response.end();
      break;
    }

    default: {
      response.writeHead(404, {
        "Content-Type": "text/html",
      });
      response.write("<h1>This doesn't exist - 404</h1>");
      response.end();
      break;
    }
  }
});

server.listen(8089);
